/* Contains a series of functions that are to be used for setting up and handling the transitions 
 * between tag clouds in our app.
 * @Author: Corey Sobel
 */

function Transitions()
{
	var _list = null;
	var _currHeight = 500;
	var _scrollorama = $.scrollorama({ blocks:'.scrollblock' });

	this.addCloud = function(tCloud,id)
	{
		addToScrollorama(_scrollorama, tCloud,id);
		//ASSUMING tCloud has class scrollback
		//placeholders
		var r = 400; 
		var centerX = 250;
		var centerY = 250;
		_currHeight += 500; //arbitrary
		var dur = 600; //how long the animation last in px
		var delay = 200;
		tCloud.children().children().each(function(idx, val)
		{
			var theta = Math.random()*2*Math.PI; //returns a value between 0 and 2PI
			var phi = Math.random()*2*Math.PI;
			var r0 =r*(Math.random()*0.5 + 1.5);
			var x0 = r0*Math.sin(theta)*Math.cos(phi);
			var y0 = r0*Math.sin(theta)*Math.sin(phi);
			var z = r0*Math.cos(theta);
			var initZoom;
			if(-0.5 < z && z < 0.5)
			{
				initZoom = 1.0;
			}
			else if(z > 0.5)
			{
				initZoom = Math.min(2,2*(z - 0.5) + 1);
			}
			else
			{
				initZoom = -0.5*(z + 0.5) + 1;
			}
			var rf = r*(Math.random()*0.9 + 0.5);
			var xf = rf*Math.sin(theta)*Math.cos(phi);
			var yf = rf*Math.sin(theta)*Math.sin(phi);
			//perform the animation call. Note: we assume default properties are as follows
			//opacity : 1, zoom : 1
			var id = '#' + $($(val).children()[0]).attr("id")
			console.log(id);
			console.log(x0 + centerX);
			console.log(xf + centerX);
			_scrollorama.animate(id, {delay:delay,duration:dur, property:'opacity', start:0});
			_scrollorama.animate(id, {delay:delay,duration:dur, property:'left', start:x0 + centerX});
		//	_scrollorama.animate(id, {duration:dur, property:'zoom', start:initZoom});
			_scrollorama.animate(id, {delay:delay,duration:dur, property:'top', start:y0 + centerY});
		});
	}
}