/* A file that contains the Content class which provides useful methods
 * for generating new tag clouds and querying the server for information
 * as needed */
 
 /* The Content class */
 function Content(trans)
 {
 	//internal counters that are used
 	//to guarentee unique names for all the dividers
 	this._cloudCounter = 0;
 	this._nodeCounter = 0;
 	this._trans = trans; //a copy of the transition utility

    //A function that takes an array of data and creates
    //a cloud for it
 	this.createCloud = function(data)
 	{
 		var str = "cnt" + this._cloudCounter;
 		var id = str;
 		this._cloudCounter++;
 		var cloud = $("<div class='scrollblock' id='"+str+"' style='width:1000px; height:1000px; background-color:#000; margin-left:auto; margin-right:auto'><ul></ul></div>");
 		for(var i = 0; i < data.length; i++)
 		{
 			var dataNode = data[i];
 			switch(dataNode.media)
 			{
 				case "img":
 					var src = dataNode.imgSrc;
 					str = "node"+this._nodeCounter;
 					this._nodeCounter++;
 					var currNode = $("<li><div id='"+str+"'><img src='"+str+"' onmouseover='' style='cursor: pointer;'></div></li>");
 					currNode.css('color','#8b0000');
 					currNode.appendTo(cloud.children()[0]);
 					currNode.click(this.registerDropboxClick(dataNode));
 					break;
 				case "file":
 					var content = dataNode.fileName;
					str = "node"+this._nodeCounter;
 					this._nodeCounter++;
 					var currNode = $("<li><div id='"+str+"' onmouseover='' style='cursor: pointer;'>"+content+"</div></li>");
 					currNode.css('color','#8B0000');
 					currNode.appendTo(cloud.children()[0]);
 					currNode.click(this.registerDropboxClick(dataNode));
 					break;
 				case "search":
 					var content = dataNode.name;
					str = "node"+this._nodeCounter;
 					this._nodeCounter++;
 					var currNode = $("<li><div id='"+str+"' onmouseover='' style='cursor: pointer;'>"+content+"</div></li>");
 					currNode.css('color','#ADD8E6'); //light blue
 					currNode.appendTo(cloud.children()[0]);
 					currNode.click(this.performFakeSearch());
 					break;
 				default:
 					console.log("ERROR!!!!!!!!!!! node not matched");
 					break;
 			}
 		}
 		this._trans.addCloud(cloud,id);
 		cloud.tagcloud({centrex:500, centrey:500, init_motion_x:10, init_motion_y:10});
 		return cloud;
 	}

 	// returns the callback function for an on click for a dropboxFile.
 	// this function creates a special cloud that contains just the original asset, 
 	// the asset name, and a button to download
 	this.registerDropboxClick = function(dataNode)
 	{

 		return function()
 		{
 			var str = "cnt" + this._cloudCounter;
 			var id = str; //for below
 			this._cloudCounter++;
 			var cloud = $("<div class='scrollblock' id='"+str+"' style='width:1000px; height:200px; background-color:#000;'><ul></ul></div>");
 			var asset;
 			if(dataNode.media == 'image')
 			{
 				var src = dataNode.imgSrc;
 				str = "node"+this._nodeCounter;
 				this._nodeCounter++;
 				asset = $("<li><div id='"+str+"'><img src='"+str+"'></div><li>");
 				var name = $("<li><div id='"+str+"' >"+dataNode.fileName+"</div></li>");
 				asset.appendTo(cloud.children()[0]);
 				name.appendTo(cloud.children()[0]);
 			}
 			else //media must be file
 			{
 				var content = dataNode.fileName;
				str = "node"+this._nodeCounter;
 				this._nodeCounter++;
 				asset = $("<li><div id='"+str+"'>"+content+"</div></li>");
 				asset.appendTo(cloud.children()[0]);
 			}
 			var downloadLink = $("<li><div id='"+str+"'><a href='"+dataNode.downloadLink+"'>Click to Download!</a></div></li>");
 			downloadLink.appendTo(cloud.children()[0]);
 			this._trans.addCloud(cloud,id);
 		}
 	}

 	//returns the callback for an node that represents and additional search
 	this.registerSearch = function(name)
 	{
 		return function()
 		{
 			this.performSearch(name);
 		}
 	}

 	     function test_content()
        {
            //first construct the JS object
            var data = new Array();
            data[0] = {media:'search',name:'cats',searchData:'n/a'};
            data[1] = {media:'search',name:'dogs',searchData:'n/a'};
            data[2] = {media:'search',name:'bonesaw',searchData:'n/a'};
            data[3] = {media:'search',name:'CMU',searchData:'n/a'};
            data[4] = {media:'search',name:'Pittsburgh',searchData:'n/a'};
            data[5] = {media:'search',name:'cats',searchData:'n/a'};
            data[6] = {media:'search',name:'dogs',searchData:'n/a'};
            data[7] = {media:'search',name:'bonesaw',searchData:'n/a'};
            data[8] = {media:'search',name:'CMU',searchData:'n/a'};
            data[9] = {media:'search',name:'Pittsburgh',searchData:'n/a'};
            data[10] = {media:'search',name:'cats',searchData:'n/a'};
            data[11] = {media:'search',name:'dogs',searchData:'n/a'};
            data[12] = {media:'search',name:'bonesaw',searchData:'n/a'};
            data[13] = {media:'search',name:'CMU',searchData:'n/a'};
            data[14] = {media:'search',name:'Pittsburgh',searchData:'n/a'};
            data[15] = {media:'search',name:'cats',searchData:'n/a'};
            data[16] = {media:'search',name:'dogs',searchData:'n/a'};
            data[17] = {media:'search',name:'bonesaw',searchData:'n/a'};
            data[18] = {media:'search',name:'CMU',searchData:'n/a'};
            data[19] = {media:'search',name:'Pittsburgh',searchData:'n/a'};
            data[20] = {media:'file',fileName:'AMERICA',downloadLink:'bogus"'}
            cont.createCloud(data);
        }

         	     function test_content2()
        {
            //first construct the JS object
            var data = new Array();
            data[0] = {media:'search',name:'cats',searchData:'n/a'};
            data[1] = {media:'search',name:'dogs',searchData:'n/a'};
            data[2] = {media:'search',name:'bonesaw',searchData:'n/a'};
            data[3] = {media:'search',name:'CMU',searchData:'n/a'};
            data[4] = {media:'search',name:'Pittsburgh',searchData:'n/a'};
            data[5] = {media:'search',name:'cats',searchData:'n/a'};
            data[6] = {media:'search',name:'dogs',searchData:'n/a'};
            data[7] = {media:'search',name:'bonesaw',searchData:'n/a'};
            data[8] = {media:'search',name:'CMU',searchData:'n/a'};
            data[9] = {media:'search',name:'Pittsburgh',searchData:'n/a'};
            data[10] = {media:'search',name:'cats',searchData:'n/a'};
            data[11] = {media:'search',name:'dogs',searchData:'n/a'};
            data[12] = {media:'search',name:'bonesaw',searchData:'n/a'};
            data[13] = {media:'search',name:'CMU',searchData:'n/a'};
            data[14] = {media:'search',name:'Pittsburgh',searchData:'n/a'};
            data[15] = {media:'search',name:'cats',searchData:'n/a'};
            data[16] = {media:'search',name:'dogs',searchData:'n/a'};
            data[17] = {media:'search',name:'bonesaw',searchData:'n/a'};
            data[18] = {media:'search',name:'CMU',searchData:'n/a'};
            data[19] = {media:'search',name:'Pittsburgh',searchData:'n/a'};
            data[20] = {media:'file',fileName:'AMERICA',downloadLink:'bogus"'}
            cont.createCloud(data);
        }

 	this.performFakeSearch = function()
 	{
 		//test_content2();
 	}

 	//This function queries the python scripts for searching and retrieving a document
 	this.performSearch = function(nameIn)
 	{
 		$.ajax({
 			url: "NAMEOFFILE.py",
 			contentType:"application/json",
 			type:"post",
 			datatype:"json",
 			data:JSON.stringify({'name':nameIn}),
 			success: function(response)
 			{
 				console.log("succesfully retrieved data from server");
 				console.log("provider is: " + response.service);
 				var res = this.createCloud(response.nodes); 
 			}
 		});
 	}
 }