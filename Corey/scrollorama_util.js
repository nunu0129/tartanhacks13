/* Adds the jquery object jObj to the scrollorama instance scrollorama
 * Does so by manipulating the blocks array in scrollorama.
 * NOTE: This should be called AFTER jObj has been added to the document but
 * BEFORE: any calls to scrollorama.animate() are made for the given object.
 * Assumes jObj corresponds to an object with id idIn
 */
function addToScrollorama(scrollorama, jObj, idIn)
{
	var scrollblocks = $('.scrollblock');
	var block = scrollblocks.eq(scrollblocks.length - 1);
	//block.css('position', 'absolute').css('top', block.top);
	scrollorama.getBlocks().push({
		block: block,
		top: block.offset().top - parseInt(block.css('margin-top'), 10),
		pin: 0,
		animations:[],
		id: idIn
	});
}

/* Removes jObj from control by scrollorama.
 * NOTE: this should be called BEFORE the jObj is deleted from the scene
 */
function removeFromScrollorama(scrollorama, jObj)
{
	var blocks = scrollorama.getBlocks();
	var removed = false;
	for(var i = 0; i < blocks.length; i++)
	{
		if(blocks[i].id == jObj)
		{
			blocks.splice(i,1);
			removed = true;
			break;
		}
	}
	return removed;
}