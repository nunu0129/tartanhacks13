/* Contains a series of functions that are to be used for setting up and handling the transitions 
 * between tag clouds in our app.
 * @Author: Corey Sobel
 */

function Transitions()
{
	this._list = null;
	this._currHeight = 500;
	this._currId = -1
	this._scrollorama = $.scrollorama({ blocks:'.scrollblock' });
	for(var i = 0; i < this._scrollorama.getBlocks().length; i++)
	{
		var block = this._scrollorama.getBlocks()[i];
		if(this._list == null)
			this._list = new Array();
		this._list.push('#'+block.id);
	}
	var _scrollorama = this._scrollorama;
	var _currId = this._currId;
	var _currHeight = this._currHeight;
	var _list = this._list;

	this.addCloud = function(tCloud,id)
	{
		//appends the tCloud
		tCloud.appendTo($('body'));
		addToScrollorama(_scrollorama, tCloud,id);
		if(this._list == null)
		{
			_list = new Array();
			this._list = _list;
		}
		if(id.indexOf('#') == -1)
		id = '#' + id;
		_list.push(id);
		//ASSUMING tCloud has class scrollback
		//placeholders
		var r = 400; 
		var centerX = 250;
		var centerY = 250;
		_currHeight += 500; //arbitrary
		var dur = 600; //how long the animation last in px
		var delay = 200;
		tCloud.children().children().each(function(idx, val)
		{
			var theta = Math.random()*2*Math.PI; //returns a value between 0 and 2PI
			var phi = Math.random()*2*Math.PI;
			var r0 =r*(Math.random()*0.5 + 1.5);
			var x0 = r0*Math.sin(theta)*Math.cos(phi);
			var y0 = r0*Math.sin(theta)*Math.sin(phi);
			var z = r0*Math.cos(theta);
			var initZoom;
			if(-0.5 < z && z < 0.5)
			{
				initZoom = 1.0;
			}
			else if(z > 0.5)
			{
				initZoom = Math.min(2,2*(z - 0.5) + 1);
			}
			else
			{
				initZoom = -0.5*(z + 0.5) + 1;
			}
			var rf = r*(Math.random()*0.9 + 0.5);
			var xf = rf*Math.sin(theta)*Math.cos(phi);
			var yf = rf*Math.sin(theta)*Math.sin(phi);
			//perform the animation call. Note: we assume default properties are as follows
			//opacity : 1, zoom : 1
			var id = '#' + $($(val).children()[0]).attr("id")
			_scrollorama.animate(id, {delay:delay,duration:dur, property:'opacity', start:0});
			_scrollorama.animate(id, {delay:delay,duration:dur, property:'left', start:x0 + centerX});
		//	_scrollorama.animate(id, {duration:dur, property:'zoom', start:initZoom});
			_scrollorama.animate(id, {delay:delay,duration:dur, property:'top', start:y0 + centerY});
		});
		if(id.indexOf('#') == -1)
			id = '#' + id;
		console.log(id);
		$('html,body').animate({scrollTop: $(id).offset().top},'slow');
		this._currId = _currId =  id;
	}
	this.remCloud = function(id)
	{
		var idx = _list.indexOf(id);
		var id2 = -1;
		if(idx != -1)
		{
			if(idx != _list.length - 1)
			{
				var id2 = _list[idx + 1];
			}
			_list.splice(idx,1);
		}
		removeFromScrollorama(_scrollorama, id); //unanimate it
		if(_list.indexOf(_currId) > idx) // _currId points to a node being deleted
		{
				if(idx > 0)
			{
				this._currId = _currId = _list[idx-1];
				$('html,body').animate({scrollTop: jQuery(_currId).offset().top},'slow');
			}
			else
			{
				console.log("WARNING: deleting from head of the list . . . ");
			}
		}
		$(id).remove();
		if(id2 != -1)
			this.remCloud(id2);
	}

		var self = this;
		//registering key presses
		$(document).keydown(function(e)
		{
			if(self._currId.indexOf('#') == -1)
				self._currId = '#' + self._currId;
			if(e.keyCode == 39 || e.keyCode == 40)
			{
				console.log("press D");
				console.log(self._list);
				var idx = self._list.indexOf(self._currId)
				if(idx < self._list.length-1)
				{
					self._currId = self._list[idx+1];
					if(self._currId.indexOf('#') == -1)
						self._currId = '#' + self._currId;
					$('html,body').animate({scrollTop: $(self._currId).offset().top},'slow');
				}
				else
				{
					console.log("NOTE: attempting to go forward while at end of webpage");
				}
			}
			else if (e.keyCode == 37 || e.keyCode == 38)
			{
				console.log(self._list);
				var idx = self._list.indexOf(self._currId);
				if(idx > 0)
				{
					self._currId = self._list[idx-1];
					if(self._currId.indexOf('#') == -1)
						self._currId = '#' + self._currId;
					$('html,body').animate({scrollTop: $(self._currId).offset().top},'slow');
					console.log(self._currId);
				}
				else
				{
					console.log("NOTE: attempting to go backward while at beginning of webpage");
				}
			}
		});
}